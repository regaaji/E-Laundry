<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    public function getSubMenu()
    {
        $query = "SELECT `user_sub_menu`.*, `user_menu`.`menu`
                FROM `user_sub_menu` JOIN `user_menu`
                ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
        ";

        return $this->db->query($query)->result_array();
    }

    public function editsubmenu($id)
	{
		return $this->db->get_where('user_sub_menu', ['id' => $id])->row_array();
	}

     public function editkritik($id)
    {
        return $this->db->get_where('tb_pesan', ['id' => $id])->row_array();
    }

     public function edittransaksiessii($id)
    {
        return $this->db->get_where('tb_transaksiEssi', ['id' => $id])->row_array();
    }


      public function edittransaksiwipe($id)
    {
        return $this->db->get_where('tb_transaksiWipe', ['id' => $id])->row_array();
    }


     public function editbarangessii($produk_id)
    {
        return $this->db->get_where('tb_produkEssi', ['produk_id' => $produk_id])->row_array();
    }

      public function editbarangwipe($produk_id)
    {
        return $this->db->get_where('tb_produkWipe', ['produk_id' => $produk_id])->row_array();
    }

    public function editbarangbasic($produk_id)
    {
        return $this->db->get_where('tb_produkBasic', ['produk_id' => $produk_id])->row_array();
    }

    public function getCountKritik()
    {
        $query = $this->db->query("SELECT * FROM tb_pesan");
        $total = $query->num_rows();
        return $total;
    }

      public function getCountWipe()
    {
        $query = $this->db->query("SELECT * FROM tb_produkWipe");
        $total = $query->num_rows();
        return $total;
    }

    public function getCountBasic()
    {
        $query = $this->db->query("SELECT * FROM tb_produkBasic");
        $total = $query->num_rows();
        return $total;
    }


      public function getCountEssii()
    {
        $query = $this->db->query("SELECT * FROM tb_produkEssi");
        $total = $query->num_rows();
        return $total;
    }
    

    public function getCountTransaksiEssii()
    {
        $query = $this->db->query("SELECT * FROM tb_transaksiEssi");
        $total = $query->num_rows();
        return $total;
    }

    public function getCountTransaksiWipe()
    {
        $query = $this->db->query("SELECT * FROM tb_transaksiWipe");
        $total = $query->num_rows();
        return $total;
    }


    public function getById($id)
    {
        return $this->db->get_where('tb_transaksiWipe', ['id' => $id])->row_array();
    }

    public function getAdminById($id)
    {
        return $this->db->get_where('tb_transaksiEssi', ['id' => $id])->row_array();
    }

    //tambah basic
    public function addbarangbasic($filename)
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "harga" => $this->input->post('harga', true),
            "gambar" => $filename,
            "paket" => $this->input->post('paket', true)  
            
        ];

        $this->db->insert('tb_produkBasic', $data);
    }


    //edit basic
       public function updatebarangbasic($produk_id, $filename)
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "harga" => $this->input->post('harga', true),
            "gambar" => $filename,
            "paket" => $this->input->post('paket', true)  
            
        ];

        $this->db->where('produk_id', $this->input->post('produk_id', true));
        $this->db->update('tb_produkBasic', $data);
    }


    public function ubahDataBarangBasic()
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "harga" => $this->input->post('harga', true),
            "paket" => $this->input->post('paket', true)
        ];  

        $this->db->where('produk_id', $this->input->post('produk_id',$id));
        $this->db->update('tb_produkBasic', $data);
    }


    // tambah wipe
      public function addbarangwipe($filename)
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "harga" => $this->input->post('harga', true),
            "gambar" => $filename,
            "paket" => $this->input->post('paket', true)  
            
        ];

        $this->db->insert('tb_produkWipe', $data);
    }

    
    //edit wipe
    public function updatebarangwipe($produk_id, $filename)
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "harga" => $this->input->post('harga', true),
            "gambar" => $filename,
            "paket" => $this->input->post('paket', true)  
            
        ];

        $this->db->where('produk_id', $this->input->post('produk_id', true));
        $this->db->update('tb_produkWipe', $data);
    }


    public function ubahDataBarangwipe()
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "harga" => $this->input->post('harga', true),
            "paket" => $this->input->post('paket', true)
        ];  

        $this->db->where('produk_id', $this->input->post('produk_id',$id));
        $this->db->update('tb_produkWipe', $data);
    }


    //tambah essii
    public function addbarang($filename)
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "harga" => $this->input->post('harga', true),
            "gambar" => $filename,
            "paket" => $this->input->post('paket', true)  
            
        ];

        $this->db->insert('tb_produkEssi', $data);
    }


    //edit essii
    public function updatebarang($produk_id, $filename)
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "harga" => $this->input->post('harga', true),
            "gambar" => $filename,
            "paket" => $this->input->post('paket', true)  
            
        ];

        $this->db->where('produk_id', $this->input->post('produk_id', true));
        $this->db->update('tb_produkEssi', $data);
    }


    public function ubahDataBarang()
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "harga" => $this->input->post('harga', true),
            "paket" => $this->input->post('paket', true)
        ];  

        $this->db->where('produk_id', $this->input->post('produk_id',$id));
        $this->db->update('tb_produkEssi', $data);
    }

}

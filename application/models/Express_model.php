<?php 

/**
 * 
 */
class Express_model extends CI_Model
{
	public function cetakpdfbasic($id)
	{
		return $this->db->get_where('tb_transaksiBasic', ['id' => $id])->row_array();
	}

	public function cetakpdf1($id)
	{
		return $this->db->get_where('tb_transaksiWipe', ['id' => $id])->row_array();
	}

	public function cetakpdf($id)
	{
		return $this->db->get_where('tb_transaksiEssi', ['id' => $id])->row_array();
	}

	public function hapuscekpesan3($id)
	{
		//$this->db->where('id', $id);
		$this->db->delete('tb_transaksiBasic', ['id' => $id]);
		 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Terima kasih telah mengunjungi website kami 
                    </div>');
	}

	public function hapuscekpesan1($id)
	{
		//$this->db->where('id', $id);
		$this->db->delete('tb_transaksiWipe', ['id' => $id]);
		 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Terima kasih telah mengunjungi website kami 
                    </div>');
	}

	public function hapuscekpesan($id)
	{
		//$this->db->where('id', $id);
		$this->db->delete('tb_transaksiEssi', ['id' => $id]);
		 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Terima kasih telah mengunjungi website kami 
                    </div>');
	}


	// public function ubahDataBukti()
	// {
	// 	$data = [
	// 		"nama" => $this->input->post('nama', true),
	// 		"telepon" => $this->input->post('telepon', true),
	// 		"tanggal_jemput" => $this->input->post('tanggal_jemput', true),
	// 		"waktu_jemput" => $this->input->post('waktu_jemput', true),
	// 		"tanggal_kirim" => $this->input->post('tanggal_kirim', true),
	// 		"waktu_kirim" => $this->input->post('waktu_kirim', true),
	// 		"location" => $this->input->post('location', true),
	// 		"alamat" => $this->input->post('alamat', true),
	// 		"harga" => $this->input->post('harga', true),
	// 		"status" => $this->input->post('status', true)
	// 	];  

	// 	$this->db->where('id', $this->input->post('id',$id));
	// 	$this->db->update('tb_transaksiEssi', $data);
	// }	

	public function updatebukti($id, $filename)
    {
        $data = [
        	"nama" => $this->input->post('nama', true),
        	"telepon" => $this->input->post('telepon', true),
        	"tanggal_jemput" => $this->input->post('tanggal_jemput', true),
        	"waktu_jemput" => $this->input->post('waktu_jemput', true),
        	"tanggal_kirim" => $this->input->post('tanggal_kirim', true),
        	"waktu_kirim" => $this->input->post('waktu_kirim', true),
        	"location" => $this->input->post('location', true),
        	"alamat" => $this->input->post('alamat', true),
        	"harga" => $this->input->post('harga', true),
        	"bukti" => $filename, 
        	"status" => $this->input->post('status', true)
            
        ];

        $this->db->where('id', $this->input->post('id', true));
        $this->db->update('tb_transaksiEssi', $data);
    }

	public function editcekpesan()
	{
		$data = [
                "nama" => $this->input->post('nama', true),
                "telepon" => $this->input->post('telepon', true),
                "tanggal_jemput" => $this->input->post('tanggal_jemput', true),
                "waktu_jemput" => $this->input->post('waktu_jemput', true),
                "tanggal_kirim" => $this->input->post('tanggal_kirim', true),
                "waktu_kirim" => $this->input->post('waktu_kirim', true),
                "location" => $this->input->post('location', true),
                "alamat" => $this->input->post('alamat', true),
                "harga" => $this->input->post('harga', true),
                "status" => $this->input->post('status', true),
                "bukti" => $this->input->post('bukti', true)

            ];  

		$this->db->where('id', $this->input->post('id',$id));
		$this->db->update('tb_transaksiEssi', $data);
			
			 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Selamat anda telah lunas 
                    </div>');
	}


	public function editcekpesan3()
	{
		$data = [
                "nama" => $this->input->post('nama', true),
                "telepon" => $this->input->post('telepon', true),
                "tanggal_jemputb" => $this->input->post('tanggal_jemputb', true),
                "waktu_jemputb" => $this->input->post('waktu_jemputb', true),
                "tanggal_kirimb" => $this->input->post('tanggal_kirimb', true),
                "waktu_kirimb" => $this->input->post('waktu_kirimb', true),
                "locationb" => $this->input->post('locationb', true),
                "alamatb" => $this->input->post('alamatb', true),
                "hargab" => $this->input->post('hargab', true),
                "statusb" => $this->input->post('statusb', true),
                "buktib" => $this->input->post('buktib', true)

            ];  

		$this->db->where('id', $this->input->post('id',$id));
		$this->db->update('tb_transaksiBasic', $data);
			
			 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Selamat anda telah lunas 
                    </div>');
	}

	public function editcekpesan1()
	{
		$data = [
                "nama" => $this->input->post('nama', true),
                "telepon" => $this->input->post('telepon', true),
                "tanggal_jemput1" => $this->input->post('tanggal_jemput1', true),
                "waktu_jemput1" => $this->input->post('waktu_jemput1', true),
                "tanggal_kirim1" => $this->input->post('tanggal_kirim1', true),
                "waktu_kirim1" => $this->input->post('waktu_kirim1', true),
                "location1" => $this->input->post('location1', true),
                "alamat1" => $this->input->post('alamat1', true),
                "harga1" => $this->input->post('harga1', true),
                "status1" => $this->input->post('status1', true),
                "bukti1" => $this->input->post('bukti1', true)

            ];  

		$this->db->where('id', $this->input->post('id',$id));
		$this->db->update('tb_transaksiWipe', $data);
			
			 $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Selamat anda telah lunas 
                    </div>');
	}

	public function getAllBasic()
	{
		$query = $this->db->query("SELECT * FROM tb_transaksiBasic");
		$total = $query->num_rows();
		return $total;
	}


	public function getAllPesan()
	{
		$query = $this->db->query("SELECT * FROM tb_transaksiEssi");
		$total = $query->num_rows();
		return $total;
	}

	public function getAllCount()
	{
		$query = $this->db->query("SELECT * FROM tb_transaksiWipe");
		$total = $query->num_rows();
		return $total;
	}

	public function getByBasicId($id)
	{
		return $this->db->get_where('tb_transaksiBasic', ['id' => $id])->row_array();
	}

	public function getById($id)
	{
		return $this->db->get_where('tb_transaksiWipe', ['id' => $id])->row_array();
	}

	public function getAdminById($id)
	{
		return $this->db->get_where('tb_transaksiEssi', ['id' => $id])->row_array();
	}
	
	function get_all_transaksiBasic(){
		 return $query = $this->db->get('tb_transaksiBasic')->result_array();
	}

	function get_all_transaksiWipe(){
		 return $query = $this->db->get('tb_transaksiWipe')->result_array();
	}

	function get_all_transaksiEssi(){
		 return $query = $this->db->get('tb_transaksiEssi')->result_array();
	}

	function get_nama($nama){
		 return $this->db->get_where('tb_transaksiEssi', ['nama' => $nama]);
	}

	function get_all_nama(){
		 return $query = $this->db->get('tb_transaksiEssi')->result_array();
	}


	
	function get_all_produk(){
		return $query = $this->db->get('tb_produkBasic')->result();
	}

	function get_all_produk1(){
		$hasil = $this->db->query("SELECT * FROM tbl_produk WHERE paket = 'wipe'");
        return $hasil->result();
	}

	function get_all_produk2(){
		$hasil = $this->db->query("SELECT * FROM tb_produkEssi WHERE paket = 'cuci_kering'");
        return $hasil->result();
	}

	function get_all_produk3(){
		$hasil = $this->db->query("SELECT * FROM tb_produkEssi WHERE paket = 'rumah_tangga'");
        return $hasil->result();
	}

	function get_all_produk4(){
		$hasil = $this->db->query("SELECT * FROM tb_produkEssi WHERE paket = 'perawatan_tas'");
        return $hasil->result();
	}

	function get_all_produkwipe(){
		return $query = $this->db->get('tb_produkWipe')->result();
	}
	
	public function tambahDataExpress()
	{
		$data = [
			"tanggal_kembali" => $this->input->post('tanggal_kembali', true),
			"waktu_kembali" => $this->input->post('waktu_kembali', true),
			"tanggal_antar" => $this->input->post('tanggal_antar', true),
			"waktu_antar" => $this->input->post('waktu_antar', true),
			"harga" => $this->input->post('harga', true),
			"lat" => $this->input->post('lat', true),
			"lng" => $this->input->post('lng', true),
			"location" => $this->input->post('location', true),
			"alamat" => $this->input->post('alamat', true),
			"paket" => $this->input->post('paket', true),

		];

		$this->session->set_userdata($data);
		$this->db->insert('tb_transaksi', $data);
	}

	

	

	public function tambahDataPremium()
	{
		$data = [
			"harga" => $this->input->post('harga', true),
			"tanggal_jemput" => $this->input->post('tanggal_jemput', true),
			"waktu_jemput" => $this->input->post('waktu_kembali', true),
			"tanggal_kirim" => $this->input->post('tanggal_kirim', true),
			"waktu_kirim" => $this->input->post('waktu_kirim', true),
			"lat" => $this->input->post('lat', true),
			"lng" => $this->input->post('lng', true),
			"location" => $this->input->post('location', true),
			"alamat" => $this->input->post('alamat', true)

		];	
		$this->session->set_userdata($data);
		var_dump($this->session->userdata($data));
		//$this->db->insert('tb_transaksi2', $data);
	}

}
<?php 

/**
 * 
 */
class Daftar_model extends CI_Model
{
	public function register($data)
	{
		return $this->db->insert('tb_konsumen', $data);
	}

	public function cekUsername($username)
	{
		return $this->db->get_where('tb_konsumen', ['username' => $username]);
	}
}
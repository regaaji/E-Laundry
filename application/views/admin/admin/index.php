

    

   

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

          <div class="row mt-5">
          	<div class="col-lg-8">
                   <?= $this->session->flashdata('message'); ?>
          		<div class="card">
          			<div class="card-body">
                  <h5 class="pb-3 font-weight-bold">Data Kritik & Saran</h5>
          				<table class="table table-hover table-responsive mt-2" id="example2">
          					<thead>
          						<tr>
          							<th scope="col">#</th>
          							<th scope="col">Pesan</th>
                        <th scope="col">Action</th>
          						</tr>
          					</thead>
          					<tbody>
                      <?php $i = 1; ?>
                      <?php foreach( $kritik as $kr ) : ?>
          						<tr>
          							<th scope="row"><?= $i; ?></th>
          							<td><?= $kr['message']; ?></td>
                        <td>
                        <a href="<?= base_url(); ?>admin/admin/editkritik/<?= $kr['id']; ?>" class="badge badge-primary">ubah</a>
                        <a href="<?= base_url(); ?>admin/admin/hapuskritik/<?= $kr['id']; ?>" class="badge badge-danger">hapus</a>
                        </td>
          						</tr>
                      <?php $i++; ?>
          						<?php endforeach; ?>
          					</tbody>
          				</table>
          			</div>
          		</div>
          	</div>


            <div class="col-lg-4">
              <div class="card">
                <div class="card-body">
                  <div id="chartContainer" style="height: 300px;">
                </div>
              </div>    
            </div>

          </div>

          
           </div>   

        
			        

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      

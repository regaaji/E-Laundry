<div class="container" style="margin: 150px auto 0;">
   <div class="row">
      <div class="col-md-8 offset-md-2">

         <div class="card shadow">
            <div class="card-header bg-primary text-white">
               <i class="fa fa-money pr-2"></i>Cek Pembayaran
            </div>
            <div class="card-body">
              <div class="alert alert-success mb-3" role="alert">
               <h4 class="alert-heading">Perhatian!</h4>
                <p>Masukkan Cetak Bukti Pembayaran Anda</p>
                <hr>
                <p class="mb-0"></p>
             </div>
             <form method="post" action="">
              <input type="hidden" name="id" value="<?= $transaksi['id']; ?>">

              <input type="hidden" class="form-control" name="nama" id="exampleInputPassword1" value="<?= $transaksi['nama']; ?>">


              <input type="hidden" class="form-control telepon" name="telepon" id="exampleInputPassword1" value="<?= $transaksi['telepon']; ?>">


              <input type="hidden" class="form-control tanggal_jemput1" name="tanggal_jemput1" id="exampleInputPassword1" value="<?= $transaksi['tanggal_jemput1']; ?>">


              <input type="hidden" class="form-control waktu_jemput1" name="waktu_jemput1" id="exampleInputPassword1" value="<?= $transaksi['waktu_jemput1']; ?>">


              <input type="hidden" class="form-control tanggal_kirim1" name="tanggal_kirim1" id="exampleInputPassword1" value="<?= $transaksi['tanggal_kirim1']; ?>">


              <input type="hidden" class="form-control waktu_kirim1" name="waktu_kirim1" id="exampleInputPassword1" value="<?= $transaksi['waktu_kirim1']; ?>">


              <input type="hidden" class="form-control location1" name="location1" id="exampleInputPassword1" value="<?= $transaksi['location1']; ?>">


              <input type="hidden" class="form-control alamat1" name="alamat1" id="exampleInputPassword1" value="<?= $transaksi['alamat1']; ?>">


              <input type="hidden" class="form-control harga1" name="harga1" id="exampleInputPassword1" value="<?= $transaksi['harga1']; ?>">


              <input type="hidden" class="form-control status1" name="status1" id="exampleInputPassword1"  value="Lunas">

              <input type="file" name="bukti1" value="<?= $transaksi['bukti1'];  ?>">
              <small id="emailHelp" class="form-text text-danger"><?= form_error('bukti1'); ?></small>

               <div class="text-center mt-3">
              <button class="btn btn-primary" type="submit">Kirim <i class="fa fa-send"></i></button>
               </div>
           </form>
        </div>
     </div>

  </div>
</div>
</div>
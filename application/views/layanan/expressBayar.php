

    <!-- Page Content -->
    <div class="container" style="margin-top: 100px;">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">
          
          <h1 class="my-4 text-primary"><img src="<?= base_url(); ?>assets/img/laundry_service.png" alt="" width="50px" height="50px">Jasa Laundry </h1>

          <!-- Blog Post -->
          <div class="card mb-4">
            
            <div class="card-body">
              
               
               
              
              <form action="" method="post">
              <h2 class="card-title text-primary mt-5">Kontak Penerima</h2>
              <hr>
              <div class="form-group">
                <label for="nama" class="font-weight-bold">Nama Penerima</label>
                <input type="text" class="form-control" id="nama" value="<?= $this->session->userdata('nama'); ?>" name="nama">
              </div>

              <div class="form-group">
                <label for="telepon" class="font-weight-bold">Nomor Handphone</label>
                <input type="text" class="form-control" id="telepon" value="<?= $this->session->userdata('telepon'); ?>" name="telepon">
              </div>
              
              
            </div>  
            
            
           
            <?php if ( isset ($this->session->userdata['masuk_in']) ) : ?>
            <a href="" data-toggle="modal" data-target="#exampleModal"><button type="button" class="btn btn-primary btn-lg float-right p-3 mb-4 mt-4" style="margin-right: 20px;">Pembayaran<i class="fa fa-arrow-right" style="padding-left: 10px;"></i></button></a>

            <!-- Modal -->
            <form action="" method="post">
               
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-money" style="padding-right: 10px;"></i>Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="card">
                      <div class="card-header bg-primary text-white font-weight-bold">
                        Metode Pembayaran
                      </div>
                      <div class="card-body">
                       <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="exampleRadios1">
                          Tunai
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="exampleRadios2">
                          Non Tunai
                        </label>
                        <label class="form-check-label float-right" for="exampleRadios2">
                          Gratis Asuransi!
                        </label>
                      </div>

                      <img src="<?= base_url() ?>assets/img/banks-image.png" class="img-fluid mt-5">

                      
                    </div>

                  </div>
                  <label for="" class="font-weight-bold mt-5" style="font-size: 20px;">Harga Total</label> 
                      <input type="text" class="form-control text-right font-weight-bold bg-white text-primary" value="Rp. 100.000" readonly>

                      <p class="text-danger mt-3"><i class="fa fa-exclamation-circle" style="padding-right: 5px;"></i>Dengan mengklik tombol pesan, anda telah menyetujui Syarat dan Ketentuan kami.</p>
                      </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="sweet()">Pesan <i class="fa fa-send"></i></button>
                  </div>
                </div>
              </div>
            </div>
            </form>

            <?php else : ?>           
             <a href="<?= base_url() ?>daftar/login"><button type="button" class="btn btn-primary btn-lg float-right p-3 mb-4 mt-4" style="margin-right: 20px;">Lanjut<i class="fa fa-arrow-right" style="padding-left: 10px;"></i></button></a>
            <?php endif; ?>
          </form>
          </div>


        </div>

        <!-- pesan -->
   <!--  <div class="chat-box fixed-positioning">
      <div class="chat-header">
        <span class="ml-5"><i class="fa fa-comment" style="padding-right: 10px;"></i>Tinggalkan Pesan</span>
        <button><i class="fa fa-comment"></i></button>
      </div>
      <div class="chat-content">
        <p class="chat-title">Mohon maaf, semua agen kami sedang sibuk. Mohon tinggalkan pesan, kami akan segera menghubungi anda.</p>
        <form action="<?= base_url(); ?>sms/send" class="chat-form" method="post">
          <div>
            <label for="name">Nomor HP Tujuan<span>*</span></label>
            <input type="text" id="name" class="form-control" required name="to">
          </div>
          <div>
            <label for="message">Pesan <span>*</span></label>
            <textarea name="message" id="message" class="form-control" name="message"></textarea>
          </div>
          <a href="" class="tombol-hapus"><button type="submit">Kirim</button></a>
        </form>
      </div>
    </div>  --> 
   <!-- akhir pesan  -->



        <!--  Widgets  -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card my-4">
            <div class="card">

              <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img src="<?= base_url(); ?>assets/img/bude.png" alt="">
                  </div>
                  <div class="carousel-item">
                    <img src="<?= base_url(); ?>assets/img/njajal.png" alt="">
                  </div>

                </div>
              </div>
            </div>
          </div>

          <!--  Widget -->
          <div class="card my-4">
            <div class="card-body">
              <h5 class="text-primary">Butuh bantuan untuk pesan?</h5>
                  <ul class="question_help_list mt-4">
                     <li class="mb-3">
                         Hubungi kami di +021-95-51-84
                     </li>
                     <li class="mb-3">
                       Email : e-laundry@email.com   
                     </li>
                     <li>
                        Senin-Jumat 8am-7pm, Sabtu/Minggu/Hari Libur 9am-6pm
                     </li>
                  </ul>
            </div>
          </div>

          <!-- Widget -->
          <div class="card my-4">
            <div class="card-body">
                <h5 class="text-primary">Nilai lebih jasa kami</h5>
                <h6 class="ml-2 font-weight-bold">Jasa Laundry Terbaik</h6>
                <ul type="circle" class="mt-3 mb-3">
                  <li class="mb-3">Praktis, bersih, dan rapi</li>
                  <li class="mb-3">Gratis antar jemput</li>
                  <li>Proses terpisah setiap laundry</li>
                </ul>

                <h6 class="ml-2 font-weight-bold mt-5">Nilai Lebih E-Laundry</h6>
                <p class="ml-2"><br /><strong>Nilai Lebih Seekmi</strong><br />- Kami hanya menggunakan pekerja profesional yang paling berpengalaman untuk memastikan kualitas layanan yang terbaik. Kepuasan pelanggan kami adalah prioritas utama kami<br />- Bebas repot, dilayani oleh customer service profesional dan ramah yang dapat dihubungi 7 hari dalam seminggu<br />- Bebas atur jadwal antar jemput pakaian</p>

                <h6 class="ml-2 font-weight-bold mt-5">Pemberitahuan</h6>
                <p class="ml-2">Pesanan akan dibatalkan secara otomatis apabila pemesan tidak dapat diverifikasi dalam kurun waktu 24 jam sejak pemesanan layanan</p>
            </div>
          </div>

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

 

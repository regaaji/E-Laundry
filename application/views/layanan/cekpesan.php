
<div class="container">
    <div class="row justify-content-center" style="margin-top: 120px;">
        <div class="col-md-10">
            <div class="card shadow">
                <div class="card-body">

                    <h3 class="text-center text-primary mb-4">Cek Pemesanan</h3>
                   <?= $this->session->flashdata('message'); ?>
                        <div class="table-responsive">
                         <table class="table table-hover">
                                  <thead class="bg-primary text-white">
                                    <tr>
                                      <th scope="col">#</th>
                                      <th scope="col">Nama</th>
                                      <th scope="col">Tanggal Penjemputan</th>
                                      <th scope="col">Tanggal Pengiriman</th>
                                      <th scope="col">Aksi</th>
                                      <th scope="col">Details</th>
                                      <th scope="col">Cetak</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php $i = 1; ?>
                                <?php foreach( $transaksi as $tr ) : ?>
                                <tr>
                                  <th scope="row"><?= $i; ?></th>
                                  <td><?= $tr['nama']; ?></td>
                                  <td><?= $tr['tanggal_jemput']; ?></td>
                                  <td><?= $tr['tanggal_kirim']; ?></td>
                                  <td>

                                    <?php if($tr['nama'] == $this->session->userdata('nama')) :  ?>
                                    
                                            <?php if(date('d-m-Y') == $tr['tanggal_kirim']) : ?>

                                                    <?php if($tr['status'] == "Belum Lunas") : ?>
                                                       <a href="<?= base_url(); ?>Cekpesan/edit/<?= $tr['id']; ?>"><button class="btn btn-danger btn-sm">Belum Lunas<i class="far fa-times-circle pl-1"></i></button></a>
                                                     <?php else : ?>
                                                      <a href="<?= base_url(); ?>Cekpesan/hapus/<?= $tr['id']; ?>"><button class="btn btn-primary btn-sm" type="button">Lunas<i class="fas fa-check-circle pl-1"></i></button></a>
                                                     <?php endif; ?>   
                                        
                                             <?php elseif(date('d-m-Y') > $tr['tanggal_kirim']) :  ?>
                                                
                                                    <?php if($tr['status'] == "Belum Lunas") : ?>
                                                       <a href="<?= base_url(); ?>Cekpesan/edit/<?= $tr['id']; ?>"><button class="btn btn-danger btn-sm">Belum Lunas<i class="far fa-times-circle pl-1"></i></button></a>
                                                     <?php else : ?>
                                                      <a href="<?= base_url(); ?>Cekpesan/hapus/<?= $tr['id']; ?>"><button class="btn btn-primary btn-sm" type="button">Lunas<i class="fas fa-check-circle pl-1"></i></button></a>
                                                     <?php endif; ?>  
  

                                              <?php else : ?>
                                        
                                                <button class="btn btn-danger btn-sm" onclick="proses()">Proses<i class="fas fa-spinner pl-1"></i></button>
                                        
                                            <?php endif; ?>
                                    
                                        <?php  else : ?> 
                                        
                                        <?php echo "Aksi"; ?>   

                                    <?php endif; ?> 


                                  </td>
                                  <td>
                                       <?php if($tr['nama'] == $this->session->userdata('nama')) :  ?>
                                        <button class="btn btn-warning btn-sm tampilModalDetailBaru" data-toggle="modal" data-target="#transaksituntas" data-id="<?=  $tr["id"]; ?>">Detail<i class="fas fa-info-circle pl-2"></i></button> 
                                        <?php else : ?>
                                        <?php echo "Detail"; ?>
                                          <?php endif; ?>  
                                  </td>
                                  <td>
                                    <?php if($tr['nama'] == $this->session->userdata('nama')) :  ?>
                                          <a href="<?= base_url(); ?>Cekpesan/cetak/<?= $tr['id']; ?>"><button class="btn btn-danger btn-sm">Cetak <i class="fa fa-print"></i></button></a>
                                     <?php else : ?>
                                     <?php echo "Cetak"; ?>
                                     <?php endif; ?> 
                                  </td>
                              </tr>
                              <script>
                                function proses(){

                                    //var njajal = '<?= $tr['tanggal_kirim']; ?>';
                                    //alert('Harus tunggu sampai dengan tanggal ' + njajal);
                                    Swal.fire(
                                        'Maaf anda harus tunggu hari ini <?= date('d-m-Y'); ?>',
                                        'Sampai tanggal pengiriman tiba',
                                        'error'
                                        )
                                }
                            </script>
                            <?php $i++; ?>
                          <?php endforeach; ?>
                          </tbody>
                      </table>
               </div>
                </div>

            </div>
        </div>
    </div>
</div>





<!-- Modal detail-->
<div class="modal fade" id="transaksituntas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail<i class="fas fa-info-circle pl-2"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
        

      <div class="row">
        <div class="col-md-6">
          <p>Nama :</p><p class="nama font-weight-bold" style="margin-top: -10px;"></p>
          <p>Telepon :</p><p class="telepon font-weight-bold" style="margin-top: -10px;"></p>
          <p>Lokasi :</p><p class="location font-weight-bold" style="margin-top: -10px;"></p>
          <p>Alamat :</p><p class="alamat font-weight-bold" style="margin-top: -10px;"></p>
          <p>Tipe Pembayaran :</p><p class="uang font-weight-bold" style="margin-top: -10px;"></p>
        </div>

        <div class="col-md-6">
        <p>Tanggal Penjemputan :</p><p class="tanggal_jemput font-weight-bold" style="margin-top: -10px;"></p>
        <p>Waktu Penjemputan :</p><p class="waktu_jemput font-weight-bold" style="margin-top: -10px;"></p>
        <p>Tanggal Pengiriman :</p><p class="tanggal_kirim font-weight-bold" style="margin-top: -10px;"></p>
        <p>Waktu Pengiriman :</p><p class="waktu_kirim font-weight-bold" style="margin-top: -10px;"></p>
        </div>
      </div>


        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </form>
    </div>
  </div>
</div>







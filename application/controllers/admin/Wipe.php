<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wipe extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'Wipe';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');
        $data['jumlah'] = $this->menu->getCountWipe();
        $data['wipe'] = $this->db->get('tb_produkWipe')->result_array();

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('harga', 'Harga', 'required|trim');
        //$this->form_validation->set_rules('gambar', 'Gambar', 'required|trim');
        $this->form_validation->set_rules('paket', 'Paket', 'required|trim');

        if ($this->form_validation->run() == false) {

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/wipe/index', $data);
            $this->load->view('admin/templates/footer');
        } else {

            $config['upload_path']           = './assets/img/wipe';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 7000;
            $config['max_width']            = 2024;
            $config['max_height']           = 2068;
               // $config['overwrite'] = true;  
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            
                if (!$this->upload->do_upload('gambar')){
                    $this->form_validation->set_rules('gambar', 'Gambar', 'required');
                    $error = array('error' => $this->upload->display_errors());
                            //return var_dump($error);
                    redirect('admin/wipe/index');


                } else {
                    $data = $this->upload->data();

                    $filename = $data['file_name'];
                    $this->menu->addbarangwipe($filename);
                      $this->session->set_flashdata('message', '
                    Data Barang ditambahkan
                    ');
                    redirect('admin/wipe/index');
                } 
         }               
    }


    public function hapus($produk_id)
    {
        //$this->db->where('id', $id);
        $this->db->delete('tb_produkWipe', ['produk_id' => $produk_id]);
         $this->session->set_flashdata('message', 'Data Barang Dihapus');
        redirect('admin/wipe/index');
    }



    public function edit($produk_id)
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['edit'] = $this->menu->editbarangwipe($produk_id);


         $data['title'] = 'Menu Edit';

          $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
          $this->form_validation->set_rules('harga', 'Harga', 'required|trim');
         // $this->form_validation->set_rules('gambar', 'Gambar', 'required|trim');
          $this->form_validation->set_rules('paket', 'Paket', 'required|trim');
     
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/wipe/edit', $data);
            $this->load->view('admin/templates/footer');
        } else {
           $config['upload_path']           = './assets/img/wipe';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 7000;
            $config['max_width']            = 2024;
            $config['max_height']           = 2068;
               // $config['overwrite'] = true;  
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($_FILES ['gambar']['name']){
                if (!$this->upload->do_upload('gambar')){
                    $this->form_validation->set_rules('gambar', 'Gambar', 'required');
                    $error = array('error' => $this->upload->display_errors());
                            //return var_dump($error);
                    redirect('admin/wipe/index');

                    //  $this->Barang_model->ubahDataBarang();
                    //  $this->session->set_flashdata('flash', 'Diubah');
                    // redirect('admin/barang/index');

                } else {
                    $data = $this->upload->data();

                    $filename = $data['file_name'];
                    $this->menu->updatebarangwipe($produk_id, $filename);
                    redirect('admin/wipe/index');
                }

            } else {

                $this->menu->ubahDataBarangwipe();
                $this->session->set_flashdata('message', '
                    Edit Menu diubah
                    ');
            redirect('admin/wipe/index');
            }
        }
    }



    public function rincilModal(){
         $this->load->model('Menu_model', 'menu');
        echo json_encode($this->menu->getById($_POST['id']));
    }


     public function hapustransaksi($id)
    {
        //$this->db->where('id', $id);
        $this->db->delete('tb_transaksiWipe', ['id' => $id]);
         $this->session->set_flashdata('message', 'Data transaksi Dihapus');
        redirect('admin/essii/transaksi');
    }


    public function transaksi()
    {
       $data['title'] = 'Wipe';
       $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->model('Menu_model', 'menu');

        $data['total'] = $this->menu->getCountTransaksiWipe();
       $data['transaksi'] = $this->db->get('tb_transaksiWipe')->result_array();
       $data['waktuA'] = ['8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00']; 

       $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
       $this->form_validation->set_rules('telepon', 'Telepon', 'required|trim');
       $this->form_validation->set_rules('tanggal_jemput1', 'Tanggal Penjemputan', 'required|trim');
       $this->form_validation->set_rules('waktu_jemput1', 'Waktu Penjemputan', 'required|trim');
       $this->form_validation->set_rules('tanggal_kirim1', 'Tanggal Pengiriman', 'required|trim');
       $this->form_validation->set_rules('waktu_kirim1', 'Waktu Pengiriman', 'required|trim');
       $this->form_validation->set_rules('location1', 'Lokasi', 'required|trim');
       $this->form_validation->set_rules('alamat1', 'Alamat', 'required|trim');
       $this->form_validation->set_rules('harga1', 'Harga', 'required|trim');
       $this->form_validation->set_rules('status1', 'Status', 'required|trim');
       $this->form_validation->set_rules('bukti1', 'Bukti', 'required|trim');
       $this->form_validation->set_rules('uang1', 'Uang', 'required|trim');
       
       if ($this->form_validation->run() == false) {
           $this->load->view('admin/templates/header', $data);
           $this->load->view('admin/templates/sidebar', $data);
           $this->load->view('admin/templates/topbar', $data);
           $this->load->view('admin/wipe/transaksi', $data);
           $this->load->view('admin/templates/footer');

       } else {
            
              $data = [ 
                "nama" => $this->input->post('nama', true),
                "telepon" => $this->input->post('telepon', true),
                "tanggal_jemput1" => $this->input->post('tanggal_jemput1', true),
                "waktu_jemput1" => $this->input->post('waktu_jemput1', true),
                "tanggal_kirim1" => $this->input->post('tanggal_kirim1', true),
                "waktu_kirim1" => $this->input->post('waktu_kirim1', true),
                "location1" => $this->input->post('location1', true),
                "alamat1" => $this->input->post('alamat1', true),
                "harga1" => $this->input->post('harga1', true),
                "status1" => $this->input->post('status1', true),
                "uang1" => $this->input->post('uang1', true)

            ];  
            $this->session->set_userdata($data);
            $this->db->insert('tb_transaksiWipe', $data);
            $this->session->set_flashdata('message', '
                    Data Transaksi Ditambahkan
                    ');
           redirect('admin/wipe/transaksi');  

       }

    }


     public function edittransaksi($id)
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['edit'] = $this->menu->edittransaksiwipe($id);

        $data['waktuA'] = ['8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00']; 

        $data['paket'] = ['Tunai', 'Non Tunai'];

         $data['title'] = 'Menu Edit';

           $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
           $this->form_validation->set_rules('telepon', 'Telepon', 'required|trim');
           $this->form_validation->set_rules('tanggal_jemput1', 'Tanggal Penjemputan', 'required|trim');
           $this->form_validation->set_rules('waktu_jemput1', 'Waktu Penjemputan', 'required|trim');
           $this->form_validation->set_rules('tanggal_kirim1', 'Tanggal Pengiriman', 'required|trim');
           $this->form_validation->set_rules('waktu_kirim1', 'Waktu Pengiriman', 'required|trim');
           $this->form_validation->set_rules('location1', 'Lokasi', 'required|trim');
           $this->form_validation->set_rules('alamat1', 'Alamat', 'required|trim');
           $this->form_validation->set_rules('harga1', 'Harga', 'required|trim');
           $this->form_validation->set_rules('status1', 'Status', 'required|trim');
           $this->form_validation->set_rules('bukti1', 'Bukti', 'required|trim');
           $this->form_validation->set_rules('uang1', 'Uang', 'required|trim');
     
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/wipe/edittransaksi', $data);
            $this->load->view('admin/templates/footer');
        } else {
            $data = [ 
                "nama" => $this->input->post('nama', true),
                "telepon" => $this->input->post('telepon', true),
                "tanggal_jemput1" => $this->input->post('tanggal_jemput1', true),
                "waktu_jemput1" => $this->input->post('waktu_jemput1', true),
                "tanggal_kirim1" => $this->input->post('tanggal_kirim1', true),
                "waktu_kirim1" => $this->input->post('waktu_kirim1', true),
                "location1" => $this->input->post('location1', true),
                "alamat1" => $this->input->post('alamat1', true),
                "harga1" => $this->input->post('harga1', true),
                "status1" => $this->input->post('status1', true),
                "uang1" => $this->input->post('uang1', true)

            ];  

             $this->db->where('id', $this->input->post('id', true));
             $this->db->update('tb_transaksiWipe', $data);
              $this->session->set_flashdata('message', '
                    Data Transaksi Diubah
                    ');
           redirect('admin/wipe/transaksi');  

        } 
           
    }
}

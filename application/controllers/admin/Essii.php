<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Essii extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {

        $data['title'] = 'Essii';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
         $data['barang'] = $this->db->get('tb_produkEssi')->result_array();

         $this->load->model('Menu_model', 'menu');

        $data['jumlah'] = $this->menu->getCountEssii();

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('harga', 'Harga', 'required|trim');
        //$this->form_validation->set_rules('gambar', 'Gambar', 'required|trim');
        $this->form_validation->set_rules('paket', 'Paket', 'required|trim');

        if ($this->form_validation->run() == false) {

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/essii/index', $data);
            $this->load->view('admin/templates/footer');
        } else {

            $config['upload_path']           = './assets/img/essii';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 7000;
            $config['max_width']            = 2024;
            $config['max_height']           = 2068;
               // $config['overwrite'] = true;  
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            
                if (!$this->upload->do_upload('gambar')){
                    $this->form_validation->set_rules('gambar', 'Gambar', 'required');
                    $error = array('error' => $this->upload->display_errors());
                            //return var_dump($error);
                    redirect('admin/essii/index');


                } else {
                    $data = $this->upload->data();

                    $filename = $data['file_name'];
                    $this->menu->addbarang($filename);
                      $this->session->set_flashdata('message', '
                    Data Barang ditambahkan
                    ');
                    redirect('admin/essii/index');
                }        
        }
    }


    public function hapus($produk_id)
    {
        //$this->db->where('id', $id);
        $this->db->delete('tb_produkEssi', ['produk_id' => $produk_id]);
         $this->session->set_flashdata('message', 'Data Barang Dihapus');
        redirect('admin/essii/index');
    }


     public function hapustransaksi($id)
    {
        //$this->db->where('id', $id);
        $this->db->delete('tb_transaksiEssi', ['id' => $id]);
         $this->session->set_flashdata('message', 'Data transaksi Dihapus');
        redirect('admin/essii/transaksi');
    }


    public function edit($produk_id)
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['edit'] = $this->menu->editbarangessii($produk_id);

        $data['paket'] = ['perawatan_tas', 'cuci_kering', 'rumah_tangga'];

         $data['title'] = 'Menu Edit';

          $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
          $this->form_validation->set_rules('harga', 'Harga', 'required|trim');
         // $this->form_validation->set_rules('gambar', 'Gambar', 'required|trim');
          $this->form_validation->set_rules('paket', 'Paket', 'required|trim');
     
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/essii/edit', $data);
            $this->load->view('admin/templates/footer');
        } else {
           $config['upload_path']           = './assets/img/essii';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 7000;
            $config['max_width']            = 2024;
            $config['max_height']           = 2068;
               // $config['overwrite'] = true;  
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($_FILES ['gambar']['name']){
                if (!$this->upload->do_upload('gambar')){
                    $this->form_validation->set_rules('gambar', 'Gambar', 'required');
                    $error = array('error' => $this->upload->display_errors());
                            //return var_dump($error);
                    redirect('admin/essii/index');

                    //  $this->Barang_model->ubahDataBarang();
                    //  $this->session->set_flashdata('flash', 'Diubah');
                    // redirect('admin/barang/index');

                } else {
                    $data = $this->upload->data();

                    $filename = $data['file_name'];
                    $this->menu->updatebarang($produk_id, $filename);
                    redirect('admin/essii/index');
                }

            } else {

                $this->menu->ubahDataBarang();
                $this->session->set_flashdata('message', '
                    Edit Menu diubah
                    ');
            redirect('admin/essii/index');
            }
        }
    }

    public function detailModal(){
         $this->load->model('Menu_model', 'menu');
          echo json_encode($this->menu->getAdminById($_POST['id']));
    }

    public function transaksi()
    {
       $data['title'] = 'Essii';
       $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

       $data['waktuA'] = ['8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00']; 

        $this->load->model('Menu_model', 'menu');

        $data['total'] = $this->menu->getCountTransaksiEssii();
       $data['transaksi'] = $this->db->get('tb_transaksiEssi')->result_array();

       $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
       $this->form_validation->set_rules('telepon', 'Telepon', 'required|trim');
       $this->form_validation->set_rules('tanggal_jemput', 'Tanggal Penjemputan', 'required|trim');
       $this->form_validation->set_rules('waktu_jemput', 'Waktu Penjemputan', 'required|trim');
       $this->form_validation->set_rules('tanggal_kirim', 'Tanggal Pengiriman', 'required|trim');
       $this->form_validation->set_rules('waktu_kirim', 'Waktu Pengiriman', 'required|trim');
       $this->form_validation->set_rules('location', 'Lokasi', 'required|trim');
       $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
       $this->form_validation->set_rules('harga', 'Harga', 'required|trim');
       $this->form_validation->set_rules('status', 'Status', 'required|trim');
       $this->form_validation->set_rules('bukti', 'Bukti', 'required|trim');
       $this->form_validation->set_rules('uang', 'Uang', 'required|trim');
       
       if ($this->form_validation->run() == false) {
           $this->load->view('admin/templates/header', $data);
           $this->load->view('admin/templates/sidebar', $data);
           $this->load->view('admin/templates/topbar', $data);
           $this->load->view('admin/essii/transaksi', $data);
           $this->load->view('admin/templates/footer');

       } else {
            
              $data = [ 
                "nama" => $this->input->post('nama', true),
                "telepon" => $this->input->post('telepon', true),
                "tanggal_jemput" => $this->input->post('tanggal_jemput', true),
                "waktu_jemput" => $this->input->post('waktu_jemput', true),
                "tanggal_kirim" => $this->input->post('tanggal_kirim', true),
                "waktu_kirim" => $this->input->post('waktu_kirim', true),
                "location" => $this->input->post('location', true),
                "alamat" => $this->input->post('alamat', true),
                "harga" => $this->input->post('harga', true),
                "status" => $this->input->post('status', true),
                "uang" => $this->input->post('uang', true)

            ];  
            $this->session->set_userdata($data);
            $this->db->insert('tb_transaksiEssi', $data);
            $this->session->set_flashdata('message', '
                    Data Transaksi Ditambahkan
                    ');
           redirect('admin/essii/transaksi');  

       }

    }


    public function edittransaksi($id)
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['edit'] = $this->menu->edittransaksiessii($id);

        $data['waktuA'] = ['8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00']; 

        $data['paket'] = ['Tunai', 'Non Tunai'];

         $data['title'] = 'Menu Edit';

           $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
           $this->form_validation->set_rules('telepon', 'Telepon', 'required|trim');
           $this->form_validation->set_rules('tanggal_jemput', 'Tanggal Penjemputan', 'required|trim');
           $this->form_validation->set_rules('waktu_jemput', 'Waktu Penjemputan', 'required|trim');
           $this->form_validation->set_rules('tanggal_kirim', 'Tanggal Pengiriman', 'required|trim');
           $this->form_validation->set_rules('waktu_kirim', 'Waktu Pengiriman', 'required|trim');
           $this->form_validation->set_rules('location', 'Lokasi', 'required|trim');
           $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
           $this->form_validation->set_rules('harga', 'Harga', 'required|trim');
           $this->form_validation->set_rules('status', 'Status', 'required|trim');
           $this->form_validation->set_rules('bukti', 'Bukti', 'required|trim');
           $this->form_validation->set_rules('uang', 'Uang', 'required|trim');
     
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/essii/edittransaksi', $data);
            $this->load->view('admin/templates/footer');
        } else {
            $data = [ 
                "nama" => $this->input->post('nama', true),
                "telepon" => $this->input->post('telepon', true),
                "tanggal_jemput" => $this->input->post('tanggal_jemput', true),
                "waktu_jemput" => $this->input->post('waktu_jemput', true),
                "tanggal_kirim" => $this->input->post('tanggal_kirim', true),
                "waktu_kirim" => $this->input->post('waktu_kirim', true),
                "location" => $this->input->post('location', true),
                "alamat" => $this->input->post('alamat', true),
                "harga" => $this->input->post('harga', true),
                "status" => $this->input->post('status', true),
                "uang" => $this->input->post('uang', true)

            ];  

             $this->db->where('id', $this->input->post('id', true));
             $this->db->update('tb_transaksiEssi', $data);
              $this->session->set_flashdata('message', '
                    Data Transaksi Diubah
                    ');
           redirect('admin/essii/transaksi');  

        } 
           
    }
}

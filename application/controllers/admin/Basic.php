<?php 

/**
 * 
 */
class Basic extends CI_Controller
{
	
	public function index()
	{
		$data['title'] = 'Basic Laundry';
		 $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		 $data['basic'] = $this->db->get('tb_produkBasic')->result_array();
		

		  $this->load->model('Menu_model', 'menu');

        $data['jumlah'] = $this->menu->getCountBasic();

        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('harga', 'Harga', 'required|trim');
        //$this->form_validation->set_rules('gambar', 'Gambar', 'required|trim');
        $this->form_validation->set_rules('paket', 'Paket', 'required|trim');

        if ($this->form_validation->run() == false) {

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/basic/index', $data);
            $this->load->view('admin/templates/footer');
        } else {

            $config['upload_path']           = './assets/img/';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 7000;
            $config['max_width']            = 2024;
            $config['max_height']           = 2068;
               // $config['overwrite'] = true;  
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            
                if (!$this->upload->do_upload('gambar')){
                    $this->form_validation->set_rules('gambar', 'Gambar', 'required');
                    $error = array('error' => $this->upload->display_errors());
                            //return var_dump($error);
                    redirect('admin/basic/index');


                } else {
                    $data = $this->upload->data();

                    $filename = $data['file_name'];
                    $this->menu->addbarangbasic($filename);
                      $this->session->set_flashdata('message', '
                    Data Barang ditambahkan
                    ');
                    redirect('admin/basic/index');
                }        
        }

	}


	 public function hapus($produk_id)
    {
        //$this->db->where('id', $id);
        $this->db->delete('tb_produkBasic', ['produk_id' => $produk_id]);
         $this->session->set_flashdata('message', 'Data Barang Dihapus');
        redirect('admin/basic/index');
    }


	public function edit($produk_id)
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Menu_model', 'menu');

        $data['edit'] = $this->menu->editbarangbasic($produk_id);


         $data['title'] = 'Menu Edit';

          $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
          $this->form_validation->set_rules('harga', 'Harga', 'required|trim');
         // $this->form_validation->set_rules('gambar', 'Gambar', 'required|trim');
          $this->form_validation->set_rules('paket', 'Paket', 'required|trim');
     
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/wipe/edit', $data);
            $this->load->view('admin/templates/footer');
        } else {
           $config['upload_path']           = './assets/img/';
            $config['allowed_types']        = 'jpg|png';
            $config['max_size']             = 7000;
            $config['max_width']            = 2024;
            $config['max_height']           = 2068;
               // $config['overwrite'] = true;  
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if($_FILES ['gambar']['name']){
                if (!$this->upload->do_upload('gambar')){
                    $this->form_validation->set_rules('gambar', 'Gambar', 'required');
                    $error = array('error' => $this->upload->display_errors());
                            //return var_dump($error);
                    redirect('admin/basic/index');

                    //  $this->Barang_model->ubahDataBarang();
                    //  $this->session->set_flashdata('flash', 'Diubah');
                    // redirect('admin/barang/index');

                } else {
                    $data = $this->upload->data();

                    $filename = $data['file_name'];
                    $this->menu->updatebarangbasic($produk_id, $filename);
                    redirect('admin/basic/index');
                }

            } else {

                $this->menu->ubahDataBarangBasic();
                $this->session->set_flashdata('message', '
                    Edit Menu diubah
                    ');
            redirect('admin/basic/index');
            }
        }
    }
}
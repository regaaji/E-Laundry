<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {

        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

         $this->load->model('Menu_model', 'menu');

        $data['jumlah'] = $this->menu->getCountKritik();

        $data['kritik'] = $this->db->get('tb_pesan')->result_array();

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/templates/sidebar', $data);
        $this->load->view('admin/templates/topbar', $data);
        $this->load->view('admin/admin/index', $data);
        $this->load->view('admin/templates/footer');
    }


    public function editkritik($id)
    {
       $data['title'] = 'Menu Edit';
       $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

       $this->load->model('Menu_model', 'menu'); 
       $data['kritik'] = $this->menu->editkritik($id);

        $this->form_validation->set_rules('message', 'Pesan', 'required|trim');
     
        if ($this->form_validation->run() == false) {
           $this->load->view('admin/templates/header', $data);
           $this->load->view('admin/templates/sidebar', $data);
           $this->load->view('admin/templates/topbar', $data);
           $this->load->view('admin/admin/editkritik', $data);
           $this->load->view('admin/templates/footer'); 
       } else {

         $data = [
            'to' => $this->input->post('to'),
            'message' => $this->input->post('message')

        ];  

        $this->db->where('id', $this->input->post('id',$id));
        $this->db->update('tb_pesan', $data);

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Menu Edit Berhasil Update 
            </div>');
        redirect('admin/admin/index');
       }
    }

    public function hapuskritik($id)
    {
        //$this->db->where('id', $id);
        $this->db->delete('tb_pesan', ['id' => $id]);
         $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                     Berhasil dihapus 
                    </div>');
        redirect('admin/admin/index');
    }


    public function user()
    {
         $data['title'] = 'My Profile';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        //echo 'Selamat Datang ' . $data['user']['name'];

        $this->load->view('admin/templates/header', $data);
        $this->load->view('admin/templates/sidebar', $data);
        $this->load->view('admin/templates/topbar', $data);
        $this->load->view('admin/admin/user', $data);
        $this->load->view('admin/templates/footer');
    }

    public function edit()
    {
         $data['title'] = 'Edit Profile';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        
         $this->form_validation->set_rules('name', 'Full Name', 'required|trim');

        if ($this->form_validation->run() == false) {

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/admin/edit', $data);
            $this->load->view('admin/templates/footer');

        } else {
            $name = $this->input->post('name');
            $email = $this->input->post('email');

            $this->db->set('name', $name);
            $this->db->where('email', $email);
            $this->db->update('user');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Profil kamu berhasil di updated
                    </div>');
                    redirect('admin/admin/user');
        }
    }


        public function changePassword()
    {

        $data['title'] = 'Change Password';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('current_password', 'Current Password', 'required|trim');
        $this->form_validation->set_rules('new_password1', 'New Password', 'required|trim|min_length[3]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'Confirm New Password', 'required|trim|min_length[3]|matches[new_password1]');

        if ($this->form_validation->run() == false) {

            $this->load->view('admin/templates/header', $data);
            $this->load->view('admin/templates/sidebar', $data);
            $this->load->view('admin/templates/topbar', $data);
            $this->load->view('admin/admin/changepassword', $data);
            $this->load->view('admin/templates/footer');
        } else {
            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password1');
            if (!password_verify($current_password, $data['user']['password'])) {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Password Salah
                    </div>');
                redirect('admin/admin/changepassword');
            } else {
                if ($current_password == $new_password) {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    Password baru harus sama dengan password lama
                    </div>');
                    redirect('admin/admin/changepassword');
                } else {
                    $password_hash = password_hash($new_password, PASSWORD_DEFAULT);

                    $this->db->set('password', $password_hash);
                    $this->db->where('email', $this->session->userdata('email'));
                    $this->db->update('user');
                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Password diubah! 
                    </div>');
                    redirect('admin/admin/changepassword');
                }
            }
        }
    }
}

<?php

/**
 * 
 */
class Layanan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Express_model');
		//$this->load->model('ExpressBayar_model');
		$this->load->model('UserModel');
	}

	public function index()
	{
		 $data['jumlah'] = $this->Express_model->getAllPesan();
		 $data['count'] = $this->Express_model->getAllCount();
		$data['judul'] = 'E-Laundry | Layanan[Satuan]';
		$data['waktuK1'] = ['8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'];
		$data['waktuA1'] = ['8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'];

		

		$this->form_validation->set_rules('subtotal', 'Subtotal', 'required');
		$this->form_validation->set_rules('tanggal_kembali1', 'Tanggal Kembali', 'required');
		$this->form_validation->set_rules('waktu_kembali1', 'Waktu Pengembalian', 'required');
		$this->form_validation->set_rules('tanggal_antar1', 'Tanggal Pengantaran', 'required');
		$this->form_validation->set_rules('waktu_antar1', 'Waktu Pengantaran', 'required');
		$this->form_validation->set_rules('lat', 'Tempat Lat', 'required');
		$this->form_validation->set_rules('lng', 'Tempat Lng', 'required');
		$this->form_validation->set_rules('location', 'Lokasi', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header', $data);
			$this->load->view('layanan/index', $data);
			$this->load->view('templates/footer');
		} else {
			//echo "ok";
			$this->Express_model->tambahDataIndex();
			redirect('ExpressBayar2/index');
		}
	}

	public function index1()
	{
		$data['judul'] = 'E-Laundry | Layanan[Kiloan]';

		$data['jumlah'] = $this->Express_model->getAllBasic();
		$data['barang'] = $this->Express_model->get_all_produk();
		$data['waktuA'] = ['8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'];


		$this->form_validation->set_rules('tanggal_jemputb', 'Tanggal Penjemputan', 'required');
        $this->form_validation->set_rules('waktu_jemputb', 'Waktu Penjemputan', 'required');
        $this->form_validation->set_rules('tanggal_kirimb', 'Tanggal Pengiriman', 'required');
        $this->form_validation->set_rules('waktu_kirimb', 'Waktu Pengiriman', 'required');
        $this->form_validation->set_rules('hargab', 'Harga', 'required');
        $this->form_validation->set_rules('latb', 'Tempat Lat', 'required');
        $this->form_validation->set_rules('lngb', 'Tempat Lng', 'required');
        $this->form_validation->set_rules('locationb', 'Lokasi', 'required');
        $this->form_validation->set_rules('alamatb', 'Alamat', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('templates/header', $data);
			$this->load->view('layanan/index1', $data);
			$this->load->view('templates/footer');
		} else {
			
			 $data = [
                "hargab" => $this->input->post('hargab', true),
                "tanggal_jemputb" => $this->input->post('tanggal_jemputb', true),
                "waktu_jemputb" => $this->input->post('waktu_jemputb', true),
                "tanggal_kirimb" => $this->input->post('tanggal_kirimb', true),
                "waktu_kirimb" => $this->input->post('waktu_kirimb', true),
                "latb" => $this->input->post('latb', true),
                "lngb" => $this->input->post('lngb', true),
                "locationb" => $this->input->post('locationb', true),
                "alamatb" => $this->input->post('alamatb', true)

            ];  
            $this->session->set_userdata($data);
            redirect('layanan/metodebayarb');
		}
	}

	 public function metodebayarb()
    {
        $data['judul'] = 'E-Laundry | MetodePembayaran';
        $this->load->view('templates/header', $data);
        $this->load->view('layanan/metodebayarb');
        $this->load->view('templates/footer');
    }
}

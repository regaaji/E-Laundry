<?php 

/**
 * 
 */
class Cekpesan1 extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Express_model');
	}
	
	public function index()
	{
		$data['transaksi'] = $this->Express_model->get_all_transaksiWipe(); 
		$data['judul'] = 'E-Laundry | Layanan[Kiloan]';

		$this->load->view('templates/header', $data);
		$this->load->view('layanan/cekpesan1', $data);
		$this->load->view('templates/footer');
	}

	public function rincilModal(){
		echo json_encode($this->Express_model->getById($_POST['id']));
	}

	public function edit($id)
	{
		$data['transaksi'] = $this->Express_model->getById($id); 
		$data['judul'] = 'E-Laundry | Cek Pemesanan'; 
		
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'required');
		$this->form_validation->set_rules('tanggal_jemput1', 'Tanggal Penjemputan', 'required');
		$this->form_validation->set_rules('waktu_jemput1', 'Waktu Penjemputan', 'required');
		$this->form_validation->set_rules('tanggal_kirim1', 'Tanggal Pengiriman', 'required');
		$this->form_validation->set_rules('waktu_kirim1', 'Waktu Penjemputan', 'required');
		$this->form_validation->set_rules('location1', 'Lokasi', 'required');
		$this->form_validation->set_rules('alamat1', 'Alamat', 'required');
		$this->form_validation->set_rules('harga1', 'Harga', 'required');
		$this->form_validation->set_rules('status1', 'Status', 'required');
		//$this->form_validation->set_rules('bukti', 'Bukti', 'required');
		
		if( $this->form_validation->run() == FALSE ){
		
		$this->load->view('templates/header', $data);
		$this->load->view('layanan/editpesan1', $data);
		$this->load->view('templates/footer');

		} else {

			$this->Express_model->editcekpesan1();		
			redirect('Cekpesan1');	
			
		}
	}

	public function hapus1($id)
	{
		$this->Express_model->hapuscekpesan1($id);

		redirect('Cekpesan1');
	}

	function cetak1($id)
	{
		$data['detail'] = $this->Express_model->cetakpdf1($id);		
		$this->load->library('pdf');
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "E-Laundry";
		$this->pdf->load_view('layanan/print1.php', $data);

	}
}
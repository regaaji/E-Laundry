<?php

class Laundry1 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Express_model');
    }
    public function index()
    {
        $data['judul'] = 'E-Laundry | Layanan[Satuan]';

        
        $data['sepatu'] = $this->Express_model->get_all_produkwipe();
        $data['waktuA'] = ['8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00']; 

        $this->form_validation->set_rules('tanggal_jemput1', 'Tanggal Penjemputan', 'required');
        $this->form_validation->set_rules('waktu_jemput1', 'Waktu Penjemputan', 'required');
        $this->form_validation->set_rules('tanggal_kirim1', 'Tanggal Pengiriman', 'required');
        $this->form_validation->set_rules('waktu_kirim1', 'Waktu Pengiriman', 'required');
        $this->form_validation->set_rules('harga1', 'Harga', 'required');
        $this->form_validation->set_rules('lat1', 'Tempat Lat', 'required');
        $this->form_validation->set_rules('lng1', 'Tempat Lng', 'required');
        $this->form_validation->set_rules('location1', 'Lokasi', 'required');
        $this->form_validation->set_rules('alamat1', 'Alamat', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('layanan/laundry1', $data);
            $this->load->view('templates/footer');
        } else {

            $data = [
                "harga1" => $this->input->post('harga1', true),
                "tanggal_jemput1" => $this->input->post('tanggal_jemput1', true),
                "waktu_jemput1" => $this->input->post('waktu_jemput1', true),
                "tanggal_kirim1" => $this->input->post('tanggal_kirim1', true),
                "waktu_kirim1" => $this->input->post('waktu_kirim1', true),
                "lat1" => $this->input->post('lat1', true),
                "lng1" => $this->input->post('lng1', true),
                "location1" => $this->input->post('location1', true),
                "alamat1" => $this->input->post('alamat1', true)

            ];  
            $this->session->set_userdata($data);
            redirect('Laundry1/metodebayar1');
        }
    }

    public function metodebayar1()
    {
        $data['judul'] = 'E-Laundry | MetodePembayaran';
        $this->load->view('templates/header', $data);
        $this->load->view('layanan/metodebayar1');
        $this->load->view('templates/footer');
    }

    
}

<?php 

/**
 * 
 */
class Cekpesan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Express_model');
	}
	
	public function index()
	{
		$data['transaksi'] = $this->Express_model->get_all_transaksiEssi(); 
		$data['judul'] = 'E-Laundry | Layanan[Kiloan]';

		$this->load->view('templates/header', $data);
		$this->load->view('layanan/cekpesan', $data);
		$this->load->view('templates/footer');
	}

	public function detailModal(){
		echo json_encode($this->Express_model->getAdminById($_POST['id']));
	}

	public function edit($id)
	{
		$data['transaksi'] = $this->Express_model->getAdminById($id); 
		$data['judul'] = 'E-Laundry | Cek Pemesanan'; 
		
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('telepon', 'Telepon', 'required');
		$this->form_validation->set_rules('tanggal_jemput', 'Tanggal Penjemputan', 'required');
		$this->form_validation->set_rules('waktu_jemput', 'Waktu Penjemputan', 'required');
		$this->form_validation->set_rules('tanggal_kirim', 'Tanggal Pengiriman', 'required');
		$this->form_validation->set_rules('waktu_kirim', 'Waktu Penjemputan', 'required');
		$this->form_validation->set_rules('location', 'Lokasi', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('harga', 'Harga', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		//$this->form_validation->set_rules('bukti', 'Bukti', 'required');
		
		if( $this->form_validation->run() == FALSE ){
		
		$this->load->view('templates/header', $data);
		$this->load->view('layanan/editpesan', $data);
		$this->load->view('templates/footer');

		} else {
			// $upload = $_FILES['bukti']['name'];
			// if($upload){
			// $config['upload_path']           = './assets/img/bukti';
   //          $config['allowed_types']        = 'doc|docx|pdf';
   //          $config['max_size']             = 0; 
   //          $this->load->library('upload', $config);
   //          $this->upload->initialize($config);
   //          	if($this->upload->do_upload('bukti')){
   //          		$new_pdf = $this->upload->data('file_name');
   //          		$this->db->set('bukti',$upload);
   //          		            $id = $this->input->post('id');
   //          $this->db->where('id',$id);
   //          $this->db->update('tb_transaksiEssi');
   //          	}else{
   //          		echo $this->upload->display_errors();
   //          	}
			// }



			$this->Express_model->editcekpesan();		
			redirect('Cekpesan');	
			
		}
	}

	public function hapus($id)
	{
		$this->Express_model->hapuscekpesan($id);

		redirect('Cekpesan');
	}

	function cetak($id)
	{
		$data['detail'] = $this->Express_model->cetakpdf($id);		
		$this->load->library('pdf');
		$this->pdf->setPaper('A4', 'potrait');
		$this->pdf->filename = "E-Laundry";
		$this->pdf->load_view('layanan/print.php', $data);

	}
}
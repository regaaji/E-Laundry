-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 06, 2019 at 07:35 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_laundry`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_produk`
--

CREATE TABLE `tbl_produk` (
  `produk_id` int(11) NOT NULL,
  `produk_nama` varchar(100) DEFAULT NULL,
  `produk_harga` double DEFAULT NULL,
  `produk_image` varchar(50) DEFAULT NULL,
  `paket` varchar(100) NOT NULL,
  `produk_deskripsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_produk`
--

INSERT INTO `tbl_produk` (`produk_id`, `produk_nama`, `produk_harga`, `produk_image`, `paket`, `produk_deskripsi`) VALUES
(1, 'Cuci & Kering', 12000, 'bedcover.jpg', 'basic', 'Tidak termasuk sprei dan sarung bantal\r\n'),
(2, 'Cuci & Setrika', 15000, 'iron.jpg', 'basic', 'Tidak termasuk sprei dan sarung bantal'),
(3, 'Fast', 45000, 'selimut.jpg', 'wipe', 'Rp. 45.00/Pair\r\n'),
(4, 'Cleaning (All Parts)', 90000, 'shoes1.jpg', 'wipe', 'Rp. 90.000/Pair'),
(5, 'Kids', 35000, 'shoes1a.jpg', 'wipe', 'Rp. 35.000/Pair'),
(6, 'Kids Cleaning (All Parts)', 70000, 'shoes1b.jpg', 'wipe', 'Rp. 70.000/Pair'),
(7, 'ES: Shirt/Batik', 30000, 'batik.jpg', 'essii_kering', 'Rp. 30.000/Pcs'),
(8, 'ES: Blazer/Jacket', 37000, 'jaket.jpg', 'essii_kering', 'Rp. 37.000/Pcs'),
(9, 'ES: Cardingan/Sweater', 31000, 'sweater.jpg', 'essii_kering', 'Rp. 31.000/Pcs'),
(10, 'ES: Pillow/Bolster', 30000, 'pillow.jpg', 'essii_rumah', 'Rp. 30.000/Pcs'),
(11, 'ES: Towel', 22000, 'towel.jpg', 'essii_rumah', 'Rp. 22.000/Pcs\r\n'),
(12, 'ES: Bag Cleaning', 80000, 'bag.jpg', 'essii_tas', 'Rp. 80.000/Pcs');

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `status` set('Aktif','Tidak Aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `username`, `password`, `gambar`, `status`) VALUES
(1, 'rega', '$2y$10$rEir11JQCDMGi3CoQqpeF.ed/2BFGUN459P1o3QcgcZnpjIQt01dC', 'admin.png', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_konsumen`
--

CREATE TABLE `tb_konsumen` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `konfirmasi_password` varchar(100) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_konsumen`
--

INSERT INTO `tb_konsumen` (`id`, `nama`, `username`, `password`, `konfirmasi_password`, `telepon`, `alamat`) VALUES
(2, 'aji', 'aji', '$2y$10$al8UPlTaqXuEX/Fipnd3xewQRwMt/Ch1KQv9df67Wibvlq0RZD3TG', '$2y$10$l//J8tf.tYLTtfQqZc8Xc.cFRsQZgWrz6AX7nX5G4PSRnq1.Spkr.', '2147483647', 'karangan'),
(4, 'sonata', 'sonata', '$2y$10$al8UPlTaqXuEX/Fipnd3xewQRwMt/Ch1KQv9df67Wibvlq0RZD3TG', '$2y$10$l//J8tf.tYLTtfQqZc8Xc.cFRsQZgWrz6AX7nX5G4PSRnq1.Spkr.', '123', 'karangsoko'),
(5, 'aan', 'aan', '$2y$10$FoIWgQGCIMO.E5tjTTBc6euYrNMcHVoNcTwLAI8eT/bbVogp3ycYa', '$2y$10$4hT7ZxadHy1Lv0ZYcz1zfuy.3DCCWZohK9LGuLoNkJ5im4cYljG4O', '123', 'karangan'),
(6, 'dody irawan', 'dody', '$2y$10$BtC8YY7yj.Jl90CVZMkFW.nxbaCh8xAgfXeRulHUPVN20cBgZ471y', '$2y$10$wXThkXvRCTA9/Art29bQB.zhee9gtbCGmxsJkPdPxDhDMuyrGHRDK', '085235149501', 'tugu'),
(7, 'rega', 'rega', '$2y$10$3ED3w/4e1l7jVQjr0eKNvezcfmkLWC.MhUR.2bbbBg74CAnXdFmb.', '$2y$10$oK6cmxtdQhiymHDW/MGHyOfIcdA4xEfzsYKkPhQQqUTT1w1Moglae', '085235149501', 'karangan');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pesan`
--

CREATE TABLE `tb_pesan` (
  `id` int(11) NOT NULL,
  `to` int(20) NOT NULL,
  `message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pesan`
--

INSERT INTO `tb_pesan` (`id`, `to`, `message`) VALUES
(35, 2147483647, 'Web ini sangat bagus dan baik'),
(37, 2147483647, 'website kurang begitu baik perlu ditingkatkan'),
(38, 2147483647, 'Website ini perlu ditingkatkan');

-- --------------------------------------------------------

--
-- Table structure for table `tb_produkBasic`
--

CREATE TABLE `tb_produkBasic` (
  `produk_id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `harga` int(100) NOT NULL,
  `paket` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produkBasic`
--

INSERT INTO `tb_produkBasic` (`produk_id`, `nama`, `gambar`, `harga`, `paket`) VALUES
(1, 'Cuci & Kering', 'bedcover.jpg', 12000, 'basic'),
(2, 'Cuci & Setrika', 'iron.jpg', 15000, 'basic');

-- --------------------------------------------------------

--
-- Table structure for table `tb_produkEssi`
--

CREATE TABLE `tb_produkEssi` (
  `produk_id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `harga` int(100) NOT NULL,
  `gambar` varchar(128) NOT NULL,
  `paket` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produkEssi`
--

INSERT INTO `tb_produkEssi` (`produk_id`, `nama`, `harga`, `gambar`, `paket`) VALUES
(1, 'ES: Bag Cleaning', 80000, 'bag1.jpg', 'perawatan_tas'),
(2, 'ES: Shirt/Batik', 30000, 'batik.jpg', 'cuci_kering'),
(3, 'ES: Blazer/Jacket', 37000, 'jaket.jpg', 'cuci_kering'),
(4, 'ES: Cardingan/Sweater', 31000, 'sweater.jpg', 'cuci_kering'),
(5, 'ES: Towel', 22000, 'towel.jpg', 'rumah_tangga'),
(6, 'ES: Pillow/Bolster', 30000, 'pillow.jpg', 'rumah_tangga');

-- --------------------------------------------------------

--
-- Table structure for table `tb_produkWipe`
--

CREATE TABLE `tb_produkWipe` (
  `produk_id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` int(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `paket` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_produkWipe`
--

INSERT INTO `tb_produkWipe` (`produk_id`, `nama`, `harga`, `gambar`, `paket`) VALUES
(1, 'Fast1', 80000, 'shoes2.jpg', 'sepatu'),
(2, 'Cleaning (All Parts)', 90000, 'shoes1.jpg', 'sepatu'),
(3, 'Kids', 35000, 'shoes1a.jpg', 'sepatu'),
(4, 'Kids Cleaning (All Parts) 	', 70000, 'shoes1b.jpg', 'sepatu');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksiBasic`
--

CREATE TABLE `tb_transaksiBasic` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `telepon` varchar(100) NOT NULL,
  `tanggal_jemputb` varchar(100) NOT NULL,
  `waktu_jemputb` varchar(100) NOT NULL,
  `tanggal_kirimb` varchar(100) NOT NULL,
  `waktu_kirimb` varchar(100) NOT NULL,
  `locationb` varchar(100) NOT NULL,
  `alamatb` varchar(100) NOT NULL,
  `hargab` varchar(100) NOT NULL,
  `statusb` varchar(100) NOT NULL,
  `buktib` varchar(100) NOT NULL,
  `uangb` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksiBasic`
--

INSERT INTO `tb_transaksiBasic` (`id`, `nama`, `telepon`, `tanggal_jemputb`, `waktu_jemputb`, `tanggal_kirimb`, `waktu_kirimb`, `locationb`, `alamatb`, `hargab`, `statusb`, `buktib`, `uangb`) VALUES
(1, 'rega', '085235149501', '01-05-2019', '10:00', '04-05-2019', '16:00', 'Trenggalek', 'trenggalek', '60000', 'Belum Lunas', '', 'Non Tunai');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksiEssi`
--

CREATE TABLE `tb_transaksiEssi` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `tanggal_jemput` varchar(50) NOT NULL,
  `waktu_jemput` varchar(50) NOT NULL,
  `tanggal_kirim` varchar(50) NOT NULL,
  `waktu_kirim` varchar(50) NOT NULL,
  `location` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  `bukti` varchar(128) NOT NULL,
  `uang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksiEssi`
--

INSERT INTO `tb_transaksiEssi` (`id`, `nama`, `telepon`, `tanggal_jemput`, `waktu_jemput`, `tanggal_kirim`, `waktu_kirim`, `location`, `alamat`, `harga`, `status`, `bukti`, `uang`) VALUES
(3, 'dody irawan', '085235149501', '28-04-2019', '10:00', '01-05-2019', '15:00', 'Trenggalek', 'trenggalek', '53000', 'Belum Lunas', 'qt5_cadaques.pdf', 'Non Tunai'),
(4, 'aan', '085235149501', '29-04-2019', '13:00', '02-05-2019', '15:00', 'Trenggalek', 'trenggalek', '53000', 'Belum Lunas', '', 'Non Tunai'),
(7, 'aji', '2147483647', '30-04-2019', '15:00', '30-04-2019', '16:00', 'Trenggalek', 'trenggalek', '80000', 'Belum Lunas', 'E-Laundry_1.pdf', 'Tunai'),
(8, 'rega', '085235149501', '02-05-2019', '9:00', '02-05-2019', '14:00', 'Trenggalek', 'trenggalek', '67000', 'Belum Lunas', '', 'Tunai');

-- --------------------------------------------------------

--
-- Table structure for table `tb_transaksiWipe`
--

CREATE TABLE `tb_transaksiWipe` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `tanggal_jemput1` varchar(50) NOT NULL,
  `waktu_jemput1` varchar(50) NOT NULL,
  `tanggal_kirim1` varchar(50) NOT NULL,
  `waktu_kirim1` varchar(50) NOT NULL,
  `location1` varchar(50) NOT NULL,
  `alamat1` varchar(50) NOT NULL,
  `harga1` varchar(50) NOT NULL,
  `status1` varchar(50) NOT NULL,
  `bukti1` varchar(50) NOT NULL,
  `uang1` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_transaksiWipe`
--

INSERT INTO `tb_transaksiWipe` (`id`, `nama`, `telepon`, `tanggal_jemput1`, `waktu_jemput1`, `tanggal_kirim1`, `waktu_kirim1`, `location1`, `alamat1`, `harga1`, `status1`, `bukti1`, `uang1`) VALUES
(2, 'rega', '085235149501', '30-04-2019', '15:00', '03-05-2019', '16:00', 'Trenggalek', 'trenggalek', '135000', 'Belum Lunas', '', 'Non Tunai');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(8, 'Rega Aji', 'regaajiprayogo23@gmail.com', 'default.jpg', '$2y$10$Z6ZpQ/tDwhElXDVHnu6Vj.4KCsRng7CaUFMg3UQUtgPDTmCSYK5VO', 1, 1, 1556192286),
(9, 'essii', 'essii@gmail.com', 'default.jpg', '$2y$10$3ED3w/4e1l7jVQjr0eKNvezcfmkLWC.MhUR.2bbbBg74CAnXdFmb.', 3, 1, 1556192286),
(10, 'Wipe', 'wipe@gmail.com', 'default.jpg', '$2y$10$3ED3w/4e1l7jVQjr0eKNvezcfmkLWC.MhUR.2bbbBg74CAnXdFmb.', 4, 1, 1556192286),
(11, 'Basic Laundry', 'basic@gmail.com', 'default.jpg', '$2y$10$3ED3w/4e1l7jVQjr0eKNvezcfmkLWC.MhUR.2bbbBg74CAnXdFmb.', 2, 1, 1556192286);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 4, 5),
(3, 1, 5),
(4, 1, 3),
(5, 1, 4),
(6, 3, 4),
(7, 1, 2),
(8, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'Basic Laundry'),
(3, 'Menu'),
(4, 'essii'),
(5, 'wipe');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'admin/admin', 'fas fa-fw fa-tachometer-alt', 1),
(2, 1, 'My Profile', 'admin/admin/user', 'fas fa-fw fa-user', 1),
(3, 1, 'Edit Profile', 'admin/admin/edit', 'fas fa-fw fa-user-edit', 1),
(5, 3, 'Submenu Management', 'admin/menu/submenu', 'fas fa-fw fa-folder-open', 1),
(6, 1, 'Change Password', 'admin/admin/changepassword', 'fas fa-fw fa-key', 1),
(7, 4, 'Barang', 'admin/essii/index', 'fas fa-fw fa-cubes', 1),
(8, 5, 'Barang', 'admin/wipe/index', 'fas fa-fw fa-cubes', 1),
(10, 5, 'Transaksi', 'admin/wipe/transaksi', 'far fa-fw fa-credit-card', 1),
(11, 2, 'Barang', 'admin/basic/index', 'fas fa-fw fa-cubes', 1),
(12, 4, 'Transaksi', 'admin/essii/transaksi', 'far fa-fw fa-credit-card', 1),
(13, 2, 'Transaksi', 'admin/basic/transaksi', 'far fa-fw fa-credit-card', 1),
(14, 1, 'Customers', 'admin/admin/customers', 'fas fa-fw fa-users', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  ADD PRIMARY KEY (`produk_id`);

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_konsumen`
--
ALTER TABLE `tb_konsumen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pesan`
--
ALTER TABLE `tb_pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_produkBasic`
--
ALTER TABLE `tb_produkBasic`
  ADD PRIMARY KEY (`produk_id`);

--
-- Indexes for table `tb_produkEssi`
--
ALTER TABLE `tb_produkEssi`
  ADD PRIMARY KEY (`produk_id`);

--
-- Indexes for table `tb_produkWipe`
--
ALTER TABLE `tb_produkWipe`
  ADD PRIMARY KEY (`produk_id`);

--
-- Indexes for table `tb_transaksiBasic`
--
ALTER TABLE `tb_transaksiBasic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_transaksiEssi`
--
ALTER TABLE `tb_transaksiEssi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_transaksiWipe`
--
ALTER TABLE `tb_transaksiWipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_produk`
--
ALTER TABLE `tbl_produk`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_konsumen`
--
ALTER TABLE `tb_konsumen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_pesan`
--
ALTER TABLE `tb_pesan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `tb_produkBasic`
--
ALTER TABLE `tb_produkBasic`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_produkEssi`
--
ALTER TABLE `tb_produkEssi`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_produkWipe`
--
ALTER TABLE `tb_produkWipe`
  MODIFY `produk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_transaksiBasic`
--
ALTER TABLE `tb_transaksiBasic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_transaksiEssi`
--
ALTER TABLE `tb_transaksiEssi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_transaksiWipe`
--
ALTER TABLE `tb_transaksiWipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
